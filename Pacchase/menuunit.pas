unit menuunit;

{$mode objfpc}{$H+}

interface

uses
  Classes, gameunit, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls;

type

  { TMenuform }

  TMenuform = class(TForm)
    Background: TImage;
    credits: TImage;
    Title: TImage;
    ngs: TImage;
    cs: TImage;
    ngd: TImage;
    cd: TImage;
    Newgame: TImage;
    procedure NewgameClick(Sender: TObject);
    procedure NewgameMouseEnter(Sender: TObject);
    procedure NewgameMouseLeave(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Menuform: TMenuform;

implementation

{$R *.lfm}

{ TMenuform }

procedure TMenuform.NewgameClick(Sender: TObject);
begin
  Self.Hide;
  Gameform.ShowModal;
  Self.show;
end;

procedure TMenuform.NewgameMouseEnter(Sender: TObject);
begin
  Newgame.Picture:=ngd.Picture;
end;

procedure TMenuform.NewgameMouseLeave(Sender: TObject);
begin
 Newgame.Picture:=ngs.Picture;
end;

end.

