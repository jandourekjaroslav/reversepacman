unit Gameunit;

{$mode objfpc}{$H+}

interface

uses
  Classes, gameobjects, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Grids,MMsystem;

type
  tghostt = (purplec, greenc, redc, pinkc);

type
  TPaint = class(TGraphicControl) // Vykreslovací kontrola

  public
    constructor Create(AOwner: TComponent; lefts, bottoms, leftsn, bottomsn: integer);
  protected
    procedure Paint; override;
    procedure DrawSquare(x: integer; y: integer; z: integer);
  private
    Sleft: integer;  //odrážka z leva
    Sbottom: integer; // odrážka ze spod
    fieldimage: TBitmap;

    monsterimagePu: TBitmap;
    monsterimageGre: TBitmap;
    monsterimagered: TBitmap;
    monsterimagepink: TBitmap;
    dotimage: TBitmap;
    cherryimage:TBitmap;
    emptyimage :Tbitmap;
    nleft: integer;
    nbottom: integer;
   public timeshift:Integer; //časový posun
    pacimageup: Tbitmap;
    pacimagedown: Tbitmap;
    pacimageleft: Tbitmap;
    pacimageright: Tbitmap;
  end;

  { TGameform }

  TGameform = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    gameoverpic: TImage;
    backgrounimage: TImage;
    emptypic: TImage;
    coefc: TLabel;
    clock1: TImage;
    clock2: TImage;
    clock3: TImage;
    clock4: TImage;
    clock: TImage;
    clocktimer: TTimer;
    life: TLabel;
    refreshtimer: TTimer;
    Winpic: TImage;
    Lives: TLabel;
    paccoord: TLabel;
    distimer: TTimer;
    upp: TImage;
    downp: TImage;
    leftp: TImage;
    rightp: TImage;
    purple: TImage;
    red: TImage;
    pink: TImage;
    greenp: TImage;
    mov1: TImage;
    mov2: TImage;
    mov3: TImage;
    mov4: TImage;
    monsterpic: TImage;
    timstopstart: TButton;
    changemonster: TButton;
    Pactimer: TTimer;
    Monstertimer: TTimer;
    procedure changetoeat; //přepisuje zobrazení důchů po snězení třešničky
    procedure Button1Click(Sender: TObject);// tlačítka ovládající šipky  (placeholder)
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject); //
    procedure changemonsterClick(Sender: TObject);//tlačítko ovládající vybrané monstrum
    procedure clockClick(Sender: TObject);
    procedure clocktimerTimer(Sender: TObject);
    procedure distimerTimer(Sender: TObject);//zobrazovací časovač
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure monsterpicClick(Sender: TObject);
    procedure MonstertimerTimer(Sender: TObject);// pohybový časovač monster
    procedure mov1Click(Sender: TObject); //obsluha šipek
    procedure mov2Click(Sender: TObject);
    procedure mov3Click(Sender: TObject);
    procedure mov4Click(Sender: TObject); //
    procedure PactimerTimer(Sender: TObject);//pohybový časovač pacmana
    procedure refreshtimerTimer(Sender: TObject);
    procedure timstopstartClick(Sender: TObject); //ovládání časovačů
    procedure ghostchangarrows(cm: Tghost);//aktualizuje šipky
    procedure mov1c(cm: Tghost); // obsluha šipek (obrázky)
    procedure mov2c(cm: Tghost);
    procedure mov3c(cm: Tghost);
    procedure mov4c(cm: Tghost); //
    procedure losecond; // kontrola výhry/prohry

  private
    { private declarations }
  public

    { public declarations }

    T: TPaint;
  end;

var
  Gameform: TGameform; //herní formulář
  curentmonstertype: Tghostt; //momentální monstrum -obrázek
   pointcount:integer; //počet zbývajících kuliček
   curclock:Integer;

implementation

{$R *.lfm}

{ TGameform }
//konstruktor Tpaint ,nastavuje základní hodnoty ,přiřazuje obrázky do promněných ze souboru
constructor TPaint.Create(AOwner: TComponent; lefts, bottoms, leftsn, bottomsn: integer);
begin
  inherited Create(AOwner);

  with GetControlClassDefaultSize do
    SetBounds(0, 0, 10000, 10000);




  fieldImage := TBitmap.Create;
  pacimageup := TBitmap.Create;
  pacimagedown := TBitmap.Create;
  pacimageleft := TBitmap.Create;
  pacimageright := TBitmap.Create;
  cherryimage:=TBitmap.Create;
  monsterimageGre:=TBitmap.Create;
  monsterimagepink:=TBitmap.Create;
  monsterimagePu:=TBitmap.Create;
  monsterimagered:=TBitmap.Create;
  dotimage := TBitmap.Create;
  emptyimage:=TBitmap.Create;
  cherryimage.LoadFromfile('..\pacman grafika\cherryimage.bmp');
  emptyimage.LoadFromFile('..\pacman grafika\emptyimage.bmp');
  fieldImage.LoadFromFile('..\pacman grafika\bacimage.bmp');
  pacimageup.LoadFromFile('..\pacman grafika\pacimageup.bmp');
  pacimagedown.LoadFromFile('..\pacman grafika\pacimagedown.bmp');
  pacimageleft.LoadFromFile('..\pacman grafika\pacimageleft.bmp');
  pacimageright.LoadFromFile('..\pacman grafika\pacimageright.bmp');
  monsterimageGre.LoadFromFile('..\pacman grafika\monimagegre.bmp');
  monsterimagePu.LoadFromFile('..\pacman grafika\monimagepu.bmp');
  monsterimagered.LoadFromFile('..\pacman grafika\monimagered.bmp');
  monsterimagepink.LoadFromFile('..\pacman grafika\monimagepink.bmp');
  dotimage.LoadFromFile('..\pacman grafika\dotimage.bmp');
  Nleft := leftsn;
  Nbottom := bottomsn;
  Sleft := lefts;
  Sbottom := bottoms;
end;

procedure TPaint.DrawSquare(x, y, z: integer);
//vykreslí jedno políčko z polí gamefiled a disfield jednotlyvých entit
var
  SHIFT_LEFT, SHIFT_BOTTOM, SIZE: integer;
var
  corner_bottom, corner_left: integer;
begin
  SHIFT_LEFT := Sleft-2;
  SHIFT_BOTTOM := Sbottom-2;
  SIZE := 40;

  corner_left := SHIFT_LEFT + x * SIZE;
  corner_bottom := SHIFT_BOTTOM - (y * SIZE);

  case z of
    0:
//prázdné políčko
    begin
      Canvas.Brush.Color := clblue;
            Canvas.Brush.Style := bsSolid;

            Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, emptyimage);
    end;
    1:
    //plné políčko -stěna
    begin
      Canvas.Brush.Color := clblue;
      Canvas.Brush.Style := bsSolid;

      Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, fieldimage);
    end;
    2:
    //plné políčko-kulička
    begin
      Canvas.Brush.Color := clWhite;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, dotimage);

    end;
    3:
    //pacman vektor y kladný (nahoru)
    begin
      Canvas.Brush.Color := clYellow;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom -timeshift - SIZE + 1, pacimageup);
    end;
    4:
    //fialové monstrum
    begin
      Canvas.Brush.Color := clPurple;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, monsterimagepu);
    end;
    5:
    //ružové monstrum
    begin
      Canvas.Brush.Color := clBlack;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, monsterimagepink);
    end;
    6:
    //zelené monstrum
    begin
      Canvas.Brush.Color := clGreen;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, monsterimageGre);
    end;
    7:
    //červené monstrum
    begin
      Canvas.Brush.Color := clred;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, monsterimagered);
    end;
    8:begin
      //pacman vektor y záporný(dolů)
      Canvas.Brush.Color := clYellow;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom +timeshift- SIZE + 1, pacimagedown);
      end;
    9:begin
      //pacman vektor x kladný (doprava)
      Canvas.Brush.Color := clYellow;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1+timeshift, corner_bottom - SIZE + 1, pacimageright);
      end;
    10:begin
      //pacman vektor x záporný(doleva)
      Canvas.Brush.Color := clYellow;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1-timeshift, corner_bottom - SIZE + 1, pacimageleft);

    end;
    11:begin
      // plné políčko -třešen
     Canvas.Brush.Color := clYellow;
      Canvas.Brush.Style := bsSolid;
      Canvas.Draw(corner_left + 1, corner_bottom - SIZE + 1, cherryimage);
    end;
  end;
end;

procedure TPaint.Paint;//rozhodovací centrum vykreslování (volá drawsquere)
var
  x1, y1: integer;
begin
  inherited Paint;
  for x1 := 0 to 20 do
    for y1 := 0 to 12 do
    begin
      if (gamefield[x1, y1, 0]) then
      begin
       // if not((((Purplem.xcurrent=x1) and (Purplem.ycurrent=y1)) or ((pinkm.xcurrent=x1) and (Pinkm.ycurrent=y1)))or(((greenm.xcurrent=x1) and (greenm.ycurrent=y1))or((redm.xcurrent=x1) and (redm.ycurrent=y1))))then
        DrawSquare(x1, y1, 1);
      end
      else if (gamefield[x1, y1, 1]) then
      begin
        DrawSquare(x1, y1, 2);
      end
      else
      if (gamefield[x1, y1, 2])then
      begin
       DrawSquare(x1, y1, 11);
      end
      else if gamefield[x1, y1, 0] = False then
      if (not (pacy.disfield[4,6]))or (Gameform.distimer.Enabled) then
        DrawSquare(x1, y1, 0)
        else if not ((x1=4)and (y1=6))then
         DrawSquare(x1, y1, 0) ;
    end;

  for x1 := 0 to 20 do
    for y1 := 0 to 12 do
    begin
      if (Pacy.disfield[x1, y1]) then
      begin
        case Pacy.lastdirection of
          up: DrawSquare(x1, y1, 3);
          down:DrawSquare(x1, y1, 8);
          lefty: DrawSquare(x1, y1, 10);
          right: DrawSquare(x1, y1, 9);
          empty: DrawSquare(x1, y1, 3);
           end;
      end;
    end;
  for x1 := 0 to 21 do
    for y1 := 0 to 12 do
    begin
      if (Purplem.disfield[x1, y1]) then
      begin
        DrawSquare(x1, y1, 4);
        ;
      end;
      if (Pinkm.disfield[x1, y1]) then
      begin
        DrawSquare(x1, y1, 5);
        ;
      end;
      if (greenm.disfield[x1, y1]) then
      begin
        DrawSquare(x1, y1, 6);
        ;
      end;
      if (redm.disfield[x1, y1]) then
      begin
        DrawSquare(x1, y1, 7);
        ;
      end;
    end;
end;

procedure TGameform.changemonsterClick(Sender: TObject);
// mění aktivní monstrum ,volá change arrows pro nové aktivní monstrum
begin
  if curentmonstertype = purplec then
  begin
    ghostchangarrows(greenm);
    curentmonstertype := greenc;
    monsterpic.Picture := greenp.Picture;
  end
  else //else
  if curentmonstertype = greenc then
  begin
    ghostchangarrows(redm);
    curentmonstertype := redc;
    monsterpic.Picture := red.Picture;
  end
  else
  if curentmonstertype = redc then
  begin
    ghostchangarrows(pinkm);
    curentmonstertype := pinkc;
    monsterpic.Picture := pink.Picture;

  end
  else
  if curentmonstertype = pinkc then
  begin
    ghostchangarrows(Purplem);
    curentmonstertype := purplec;
    monsterpic.Picture := purple.Picture;

  end;

end;

procedure TGameform.clockClick(Sender: TObject);
begin
 timstopstart.OnClick(sender);
end;

procedure TGameform.clocktimerTimer(Sender: TObject);
begin

  if curclock= 1 then begin curclock:=2; clock.Picture:= clock1.Picture; end else
   if curclock= 2 then begin curclock:=3; clock.Picture:= clock2.Picture; end else
   if curclock= 3 then begin curclock:=4;  clock.Picture:= clock3.Picture; end else
   if curclock= 4 then begin curclock:=1;  clock.Picture:= clock4.Picture; end;

end;

procedure TGameform.distimerTimer(Sender: TObject);
//zobrazovací časovač ,změna timeshift,zmněna obrázku pacmana(otevřená zavřená pusa)
begin
  if T.timeshift=0 then begin
  T.timeshift:=1 ;
  T.pacimageup.LoadFromFile('..\pacman grafika\pacimageclosed.bmp');
  T.pacimagedown.LoadFromFile('..\pacman grafika\pacimageclosed.bmp');
  T.pacimageleft.LoadFromFile('..\pacman grafika\pacimageclosed.bmp');
  T.pacimageright.LoadFromFile('..\pacman grafika\pacimageclosed.bmp');
  end
  else begin
  T.timeshift:=0;
  T.pacimageup.LoadFromFile('..\pacman grafika\pacimageup.bmp');
  T.pacimagedown.LoadFromFile('..\pacman grafika\pacimagedown.bmp');
  T.pacimageleft.LoadFromFile('..\pacman grafika\pacimageleft.bmp');
  T.pacimageright.LoadFromFile('..\pacman grafika\pacimageright.bmp');

  end;
  //PlaySound('pacsound', 0, SND_ASYNC);
  losecond;
    gamefield[10, 5,0] := True;
  gamefield[10, 7,0] := True;
  gamefield[9, 6,0] := True;
  gamefield[11, 6,0] := True;
  T.Invalidate;
  T.Invalidate;
  life.Caption:=IntToStr(Pacy.lives);
end;

procedure TGameform.Button1Click(Sender: TObject);
//place holder
begin
  case curentmonstertype of
    purplec: mov1c(Purplem);
    greenc: mov1c(greenm);
    redc: mov1c(redm);
    pinkc: mov1c(pinkm);
  end;
  Lives.Caption := ('1');
end;

procedure TGameform.Button2Click(Sender: TObject);
//place holder
begin
  case curentmonstertype of
    purplec: mov2c(Purplem);
    greenc: mov2c(greenm);
    redc: mov2c(redm);
    pinkc: mov2c(pinkm);
  end;
end;

procedure TGameform.Button3Click(Sender: TObject);
//place holder
begin
  case curentmonstertype of
    purplec: mov3c(Purplem);
    greenc: mov3c(greenm);
    redc: mov3c(redm);
    pinkc: mov3c(pinkm);
  end;
end;

procedure TGameform.Button4Click(Sender: TObject);
//place holder
begin
  case curentmonstertype of
    purplec: mov4c(Purplem);
    greenc: mov4c(greenm);
    redc: mov4c(redm);
    pinkc: mov4c(pinkm);
  end;
end;

procedure TGameform.FormCreate(Sender: TObject);

//inicializace programu ,vytváří instance a nastavuje hodnoty promněných a položek v poli
var
  x, x1, y1, y: integer;
begin
  curclock:=1;
  pointcount:=170;
  Purplem := Tghost.Create(10, 5, 0, 0, False);
  redm := Tghost.Create(11, 6, 0, 0, False);
  greenm := Tghost.Create(9, 6, 0, 0, False);
  pinkm := Tghost.Create(10, 7, 0, 0, False);
  Pacy := Tpac.Create(4, 6, 0, 0, False);
  T := TPaint.Create(self, 92, 728, 10, 12);
  T.parent := self;
  T.Top := 0;
  T.Left := 0;
  T.SetZOrder(False);
  curentmonstertype := purplec;
  Pacy.lastdirection:=empty;
  Purplem.Move1 := down;
  Purplem.Move2 := empty;
  Purplem.Move3 := empty;
  Purplem.Move4 := empty;

  greenm.Move1 := lefty;
  greenm.Move2 := empty;
  greenm.Move3 := empty;
  greenm.Move4 := empty;

  pinkm.Move1 := up;
  pinkm.Move2 := empty;
  pinkm.Move3 := empty;
  pinkm.Move4 := empty;

  redm.Move1 := right;
  redm.Move2 := empty;
  redm.Move3 := empty;
  redm.Move4 := empty;

  Purplem.lastdirection:=empty;
  redm.lastdirection:=empty;
  greenm.lastdirection:=empty;
  pinkm.lastdirection:=empty;
  for x := 0 to 20 do
    for y := 0 to 12 do
    begin
      gamefield[x, y, 0] := False;
    end;


  gamefield[0, 3, 0] := True;
  gamefield[0, 9, 0] := True;

  gamefield[1, 1, 0] := True;
  gamefield[1, 3, 0] := True;
  gamefield[1, 5, 0] := True;
  gamefield[1, 6, 0] := True;
  gamefield[1, 7, 0] := True;
  gamefield[1, 9, 0] := True;
  gamefield[1, 11, 0] := True;

  gamefield[2, 1, 0] := True;
  gamefield[2, 11, 0] := True;

  gamefield[3, 1, 0] := True;
  gamefield[3, 3, 0] := True;
  gamefield[3, 4, 0] := True;
  gamefield[3, 5, 0] := True;
  gamefield[3, 7, 0] := True;
  gamefield[3, 8, 0] := True;
  gamefield[3, 9, 0] := True;
  gamefield[3, 11, 0] := True;

  gamefield[5, 0, 0] := True;
  gamefield[5, 1, 0] := True;
  gamefield[5, 2, 0] := True;
  gamefield[5, 4, 0] := True;
  gamefield[5, 5, 0] := True;
  gamefield[5, 6, 0] := True;
  gamefield[5, 7, 0] := True;
  gamefield[5, 8, 0] := True;
  gamefield[5, 10, 0] := True;
  gamefield[5, 11, 0] := True;

  gamefield[6, 5, 0] := True;
  gamefield[6, 6, 0] := True;

  gamefield[7, 1, 0] := True;
  gamefield[7, 3, 0] := True;
  gamefield[7, 5, 0] := True;
  gamefield[7, 6, 0] := True;
  gamefield[7, 8, 0] := True;
  gamefield[7, 9, 0] := True;
  gamefield[7, 11, 0] := True;
  gamefield[7, 12, 0] := True;

  gamefield[8, 9, 0] := True;
  gamefield[8, 3, 0] := True;
  gamefield[8, 1, 0] := True;

  gamefield[9, 3, 0] := True;
  gamefield[9, 5, 0] := True;
  gamefield[9, 7, 0] := True;
  gamefield[9, 9, 0] := True;
  gamefield[9, 11, 0] := True;

  gamefield[10, 0, 0] := True;
  gamefield[10, 1, 0] := True;
  gamefield[10, 6, 0] := True;
  gamefield[10, 11, 0] := True;

  gamefield[11, 3, 0] := True;
  gamefield[11, 5, 0] := True;
  gamefield[11, 7, 0] := True;
  gamefield[11, 9, 0] := True;
  gamefield[11, 11, 0] := True;

  gamefield[12, 9, 0] := True;
  gamefield[12, 3, 0] := True;
  gamefield[12, 1, 0] := True;

  gamefield[13, 1, 0] := True;
  gamefield[13, 3, 0] := True;
  gamefield[13, 5, 0] := True;
  gamefield[13, 6, 0] := True;
  gamefield[13, 8, 0] := True;
  gamefield[13, 9, 0] := True;
  gamefield[13, 11, 0] := True;
  gamefield[13, 12, 0] := True;

  gamefield[14, 5, 0] := True;
  gamefield[14, 6, 0] := True;

  gamefield[15, 0, 0] := True;
  gamefield[15, 1, 0] := True;
  gamefield[15, 2, 0] := True;
  gamefield[15, 4, 0] := True;
  gamefield[15, 5, 0] := True;
  gamefield[15, 6, 0] := True;
  gamefield[15, 7, 0] := True;
  gamefield[15, 8, 0] := True;
  gamefield[15, 10, 0] := True;
  gamefield[15, 11, 0] := True;

  gamefield[17, 1, 0] := True;
  gamefield[17, 3, 0] := True;
  gamefield[17, 4, 0] := True;
  gamefield[17, 5, 0] := True;
  gamefield[17, 7, 0] := True;
  gamefield[17, 8, 0] := True;
  gamefield[17, 9, 0] := True;
  gamefield[17, 11, 0] := True;

  gamefield[18, 1, 0] := True;
  gamefield[18, 11, 0] := True;

  gamefield[19, 1, 0] := True;
  gamefield[19, 3, 0] := True;
  gamefield[19, 5, 0] := True;
  gamefield[19, 6, 0] := True;
  gamefield[19, 7, 0] := True;
  gamefield[19, 9, 0] := True;
  gamefield[19, 11, 0] := True;

  gamefield[20, 3, 0] := True;
  gamefield[20, 9, 0] := True;

  for x1 := 0 to 20 do
    for y1 := 0 to 12 do
    begin
      if gamefield[x1, y1, 0] = False then
        gamefield[x1, y1, 1] := True;

    end;
  gamefield[4, 6, 1] := False;
  gamefield[10, 5, 1] := False;
  gamefield[10, 7, 1] := False;
  gamefield[9, 6, 1] := False;
  gamefield[11, 6, 1] := False;

   gamefield[0, 0, 1] := False;
   gamefield[0, 12, 1] := False;
   gamefield[20, 0, 1] := False;
   gamefield[20, 12, 1] := False;
    for x1 := 0 to 20 do
    for y1 := 0 to 12 do
     gamefield[x1, y1, 2] := false;

    gamefield[0, 0, 2] := True;
   gamefield[0, 12, 2] := True;
   gamefield[20, 0, 2] := True;
   gamefield[20, 12, 2] :=True;

  gamefield[10, 5, 0] := True;
  gamefield[10, 7, 0] := True;
  gamefield[9, 6, 0] := True;
  gamefield[11, 6, 0] := True;
  Pacy.disfield[4, 6] := True;
  Purplem.disfield[10, 5] := True;
  pinkm.disfield[10, 7] := True;
  greenm.disfield[9, 6] := True;
  redm.disfield[11, 6] := True;
  Pacy.lives:=5;
  Pacy.coef:=1;
end;

procedure TGameform.FormShow(Sender: TObject);
begin

end;

procedure TGameform.monsterpicClick(Sender: TObject);
begin
  changemonster.OnClick(sender);
end;



procedure TGameform.MonstertimerTimer(Sender: TObject);
// zařizuje spouštění pohybu monster ,promazává jejich zobrazovací pole a deklaruje nové hodnoty ,spouští změnu šipek
var
  x1, y1: integer;
begin
  for x1 := 0 to 20 do
    for y1 := 0 to 12 do
      pinkm.disfield[x1, y1] := False;
  for x1 := 0 to 20 do
    for y1 := 0 to 12 do
      greenm.disfield[x1, y1] := False;
  for x1 := 0 to 20 do
    for y1 := 0 to 12 do
      Purplem.disfield[x1, y1] := False;
  for x1 := 0 to 20 do
    for y1 := 0 to 12 do
      redm.disfield[x1, y1] := False;

  pinkm.Move;

  redm.Move;

  Purplem.Move;

  greenm.Move;
   case curentmonstertype of
    purplec: ghostchangarrows(Purplem);
    greenc: ghostchangarrows(greenm);
    redc: ghostchangarrows(redm);
    pinkc: ghostchangarrows(pinkm);
   end;

  pinkm.disfield[pinkm.xcurrent, pinkm.ycurrent] := True;
  redm.disfield[redm.xcurrent, redm.ycurrent] := True;
  greenm.disfield[greenm.xcurrent, greenm.ycurrent] := True;
  Purplem.disfield[Purplem.xcurrent, Purplem.ycurrent] := True;
   losecond;
end;

procedure TGameform.mov1Click(Sender: TObject);
 //spustí změnu prvního pohybu aktualního monstra
begin
  case curentmonstertype of
    purplec: mov1c(Purplem);
    greenc: mov1c(greenm);
    redc: mov1c(redm);
    pinkc: mov1c(pinkm);
  end;


end;

procedure TGameform.mov1c(cm: Tghost);
//změní první puhyb aktualního monstra
begin
  if cm.Move1 = up then
  begin
    cm.Move1 := right;
    mov1.Picture := rightp.Picture;
  end
  else
  if cm.Move1 = right then
  begin
    cm.Move1 := down;
    mov1.Picture := downp.Picture;
  end
  else
  if cm.Move1 = down then
  begin
    cm.Move1 := lefty;
    mov1.Picture := leftp.Picture;
  end
  else
  if cm.Move1 = lefty then
  begin
    cm.Move1 := up;
    mov1.Picture := upp.Picture;
  end else
  if cm.Move1=empty then
  begin
  cm.Move1 := up;
    mov1.Picture := upp.Picture;
  end;
end;

procedure TGameform.ghostchangarrows(cm: Tghost);
//změní šipky pro aktualní monstrum
begin
  case cm.Move1 of
    up: mov1.Picture := upp.Picture;
    down: mov1.Picture := downp.Picture;
    lefty: mov1.Picture := leftp.Picture;
    right: mov1.Picture := rightp.Picture;
    empty: mov1.Picture:=emptypic.Picture;
  end;
  case cm.Move2 of
    up: mov2.Picture := upp.Picture;
    down: mov2.Picture := downp.Picture;
    lefty: mov2.Picture := leftp.Picture;
    right: mov2.Picture := rightp.Picture;
    empty: mov2.Picture:=emptypic.Picture;
  end;
  case cm.Move3 of
    up: mov3.Picture := upp.Picture;
    down: mov3.Picture := downp.Picture;
    lefty: mov3.Picture := leftp.Picture;
    right: mov3.Picture := rightp.Picture;
    empty: mov3.Picture:=emptypic.Picture;
  end;
  case cm.Move4 of
    up: mov4.Picture := upp.Picture;
    down: mov4.Picture := downp.Picture;
    lefty: mov4.Picture := leftp.Picture;
    right: mov4.Picture := rightp.Picture;
    empty: mov4.Picture:=emptypic.Picture;
  end;
end;

procedure TGameform.mov2Click(Sender: TObject);
//spustí změnu druhého pohybu aktualního monstra
//změní druhý puhyb aktualního monstra
begin
  case curentmonstertype of
    purplec: mov2c(Purplem);
    greenc: mov2c(greenm);
    redc: mov2c(redm);
    pinkc: mov2c(pinkm);
  end;
end;

procedure TGameform.mov2c(cm: Tghost);
//změní druhý puhyb aktualního monstra
begin
  if cm.Move2 = up then
  begin
    cm.Move2 := right;
    mov2.Picture := rightp.Picture;
  end
  else
  if cm.Move2 = right then
  begin
    cm.Move2 := down;
    mov2.Picture := downp.Picture;
  end
  else
  if cm.Move2 = down then
  begin
    cm.Move2 := lefty;
    mov2.Picture := leftp.Picture;
  end
  else
  if cm.Move2 = lefty then
  begin
    cm.Move2 := up;
    mov2.Picture := upp.Picture;
  end else
  if cm.Move2 =empty then
  begin
     cm.Move2 := up;
    mov2.Picture := upp.Picture;
  end;
end;

procedure TGameform.mov3Click(Sender: TObject);
//spustí změnu třetího pohybu aktualního monstra

begin
  case curentmonstertype of
    purplec: mov3c(Purplem);
    greenc: mov3c(greenm);
    redc: mov3c(redm);
    pinkc: mov3c(pinkm);
  end;

end;

procedure TGameform.mov3c(cm: Tghost);
//změní třetí puhyb aktualního monstra
begin
  if cm.Move3 = up then
  begin
    cm.Move3 := right;
    mov3.Picture := rightp.Picture;
  end
  else
  if cm.Move3 = right then
  begin
    cm.Move3 := down;
    mov3.Picture := downp.Picture;
  end
  else
  if cm.Move3 = down then
  begin
    cm.Move3 := lefty;
    mov3.Picture := leftp.Picture;
  end
  else
  if cm.Move3 = lefty then
  begin
    cm.Move3 := up;
    mov3.Picture := upp.Picture;
  end else
  if cm.Move3 =empty then
  begin
     cm.Move3 := up;
    mov3.Picture := upp.Picture;
  end;
end;


procedure TGameform.mov4Click(Sender: TObject);
//spustí změnu čtvrtého pohybu aktualního monstra

begin
  case curentmonstertype of
    purplec: mov4c(Purplem);
    greenc: mov4c(greenm);
    redc: mov4c(redm);
    pinkc: mov4c(pinkm);
  end;

end;
procedure TGameform.mov4c(cm: Tghost);
//změní čtvrtý puhyb aktualního monstra
begin
  if cm.Move4 = up then
  begin
    cm.Move4 := right;
    mov4.Picture := rightp.Picture;
  end
  else
  if cm.Move4 = right then
  begin
    cm.Move4 := down;
    mov4.Picture := downp.Picture;
  end
  else
  if cm.Move4 = down then
  begin
    cm.Move4 := lefty;
    mov4.Picture := leftp.Picture;
  end
  else
  if cm.Move4 = lefty then
  begin
    cm.Move4 := up;
    mov4.Picture := upp.Picture;
  end
  else
    if cm.Move4=empty then
    begin
    cm.Move4:=up;
    mov4.Picture:=upp.Picture;

  end ;

end;
procedure TGameform.changetoeat;
//změní obrázky monster po aktivaci třešně
begin
 if Pacy.coef>1 then begin
  T.monsterimageGre.loadfromfile('..\pacman grafika\monimagegreeat.bmp');
  T.monsterimagePu.LoadFromFile('..\pacman grafika\monimagepueat.bmp');
  T.monsterimagered.LoadFromFile('..\pacman grafika\monimageredeat.bmp');
  T.monsterimagepink.LoadFromFile('..\pacman grafika\monimagepinkeat.bmp');
 end else begin
   T.monsterimageGre.LoadFromFile('..\pacman grafika\monimagegre.bmp');
  T.monsterimagePu.LoadFromFile('..\pacman grafika\monimagepu.bmp');
  T.monsterimagered.LoadFromFile('..\pacman grafika\monimagered.bmp');
  T.monsterimagepink.LoadFromFile('..\pacman grafika\monimagepink.bmp');
 end;
end;

procedure TGameform.PactimerTimer(Sender: TObject);
var x1,y1 :integer;

begin

  for x1 := 0 to 20 do begin
    pacy.disfield[x1, 0] := False;
    for y1 := 0 to 12 do begin
      pacy.disfield[0, y1] := False;
      pacy.disfield[x1, y1] := False;

  end;
  end;
  Pacy.Move;
   pacy.disfield[pacy.xcurrent, pacy.ycurrent] := True;
   if gamefield[pacy.xcurrent,Pacy.ycurrent,1]= true then
   begin
     gamefield[pacy.xcurrent,Pacy.ycurrent,1]:=false;
     pointcount:=pointcount-1;
   end;
   if gamefield[pacy.xcurrent,Pacy.ycurrent,2]= true then  begin
   gamefield[pacy.xcurrent,Pacy.ycurrent,2]:=false;
   if pacy.coef=1 then
   begin
     gamefield[pacy.xcurrent,Pacy.ycurrent,2]:=false;
     pointcount:=pointcount-1;
     pacy.coef:=30;

   end;

   end;
   if Pacy.coef>1 then begin
     pacy.coef:=Pacy.coef-1;
   end;
    changetoeat;
   losecond;
  paccoord.Caption:=(IntToStr(Pacy.xcurrent)+ IntToStr(Pacy.ycurrent));
  if Pacy.coef=1 then coefc.Visible:=false
  else coefc.Visible:=true;

  coefc.Caption:=inttostr(Pacy.coef);

end;

procedure TGameform.refreshtimerTimer(Sender: TObject);
begin
  t.Invalidate;
end;

procedure TGameform.losecond;
begin
    if  pointcount<1 then begin
      Pactimer.Enabled:=false;
      Monstertimer.Enabled:=false;
      clocktimer.Enabled:=false;
      T.Visible:=false;
      gameoverpic.Visible:=true;

  end else
  if pacy.lives =0 then begin
    distimer.Enabled:=false;
    Pactimer.Enabled:=false;
      Monstertimer.Enabled:=false;
      clocktimer.Enabled:=false;
      T.Visible:=false;
      Winpic.Visible:=true;
  end else
  if Pacy.coef=1 then
  begin
    if (pacy.xcurrent=Purplem.xcurrent) and (Pacy.ycurrent=Purplem.ycurrent) then
  begin
   Pacy.disfield[4, 6] := True;
  Purplem.disfield[10, 5] := True;
  pinkm.disfield[10, 7] := True;
  greenm.disfield[9, 6] := True;
  redm.disfield[11, 6] := True;
  pacy.disfield[Pacy.xcurrent, pacy.ycurrent]:=false;
  Purplem.disfield[Purplem.xcurrent,Purplem.ycurrent]:=false;
  pinkm.disfield[pinkm.xcurrent,pinkm.ycurrent]:=false;
  greenm.disfield[greenm.xcurrent,greenm.ycurrent]:=false;
  redm.disfield[redm.xcurrent,redm.ycurrent]:=false;
  Purplem.lastdirection:=empty;
  pinkm.lastdirection:=empty;
  greenm.lastdirection:=empty;
  redm.lastdirection:=empty;
  pacy.xcurrent:=4;
  Pacy.ycurrent:=6;
  Purplem.xcurrent:=10;
  Purplem.ycurrent:=5;
  pinkm.xcurrent:=10;
  pinkm.ycurrent:=7;
  greenm.xcurrent:=9;
  greenm.ycurrent:=6;
  redm.xcurrent:=11;
  redm.ycurrent:=6;
  Monstertimer.Enabled:=false;
  Pactimer.Enabled:=false;
  distimer.Enabled:=false;
  clocktimer.Enabled:=false;
    gamefield[10, 5,0] := false;
  gamefield[10, 7,0] := false;
  gamefield[9, 6,0] := false;
  gamefield[11, 6,0] := false;
  pacy.lives:=Pacy.lives-1;
  end;
   if (pacy.xcurrent=greenm.xcurrent) and (Pacy.ycurrent=greenm.ycurrent) then
  begin
    Pacy.disfield[4, 6] := True;
  Purplem.disfield[10, 5] := True;
  pinkm.disfield[10, 7] := True;
  greenm.disfield[9, 6] := True;
  redm.disfield[11, 6] := True;
  pacy.disfield[Pacy.xcurrent, pacy.ycurrent]:=false;
  Purplem.disfield[Purplem.xcurrent,Purplem.ycurrent]:=false;
  pinkm.disfield[pinkm.xcurrent,pinkm.ycurrent]:=false;
  greenm.disfield[greenm.xcurrent,greenm.ycurrent]:=false;
  redm.disfield[redm.xcurrent,redm.ycurrent]:=false;
   Purplem.lastdirection:=empty;
  pinkm.lastdirection:=empty;
  greenm.lastdirection:=empty;
  redm.lastdirection:=empty;
  pacy.xcurrent:=4;
  Pacy.ycurrent:=6;
  Purplem.xcurrent:=10;
  Purplem.ycurrent:=5;
  pinkm.xcurrent:=10;
  pinkm.ycurrent:=7;
  greenm.xcurrent:=9;
  greenm.ycurrent:=6;
  redm.xcurrent:=11;
  redm.ycurrent:=6;
  Monstertimer.Enabled:=false;
  Pactimer.Enabled:=false;
  distimer.Enabled:=false;
     gamefield[10, 5,0] := false;
  gamefield[10, 7,0] := false;
  gamefield[9, 6,0] := false;
  gamefield[11, 6,0] := false;
  pacy.lives:=Pacy.lives-1;
  clocktimer.Enabled:=false;
  end;
    if (pacy.xcurrent=redm.xcurrent) and (Pacy.ycurrent=redm.ycurrent) then
  begin
   Pacy.disfield[4, 6] := True;
  Purplem.disfield[10, 5] := True;
  pinkm.disfield[10, 7] := True;
  greenm.disfield[9, 6] := True;
  redm.disfield[11, 6] := True;
  pacy.disfield[Pacy.xcurrent, pacy.ycurrent]:=false;
  Purplem.disfield[Purplem.xcurrent,Purplem.ycurrent]:=false;
  pinkm.disfield[pinkm.xcurrent,pinkm.ycurrent]:=false;
  greenm.disfield[greenm.xcurrent,greenm.ycurrent]:=false;
  redm.disfield[redm.xcurrent,redm.ycurrent]:=false;
   Purplem.lastdirection:=empty;
  pinkm.lastdirection:=empty;
  greenm.lastdirection:=empty;
  redm.lastdirection:=empty;
  pacy.xcurrent:=4;
  Pacy.ycurrent:=6;
  Purplem.xcurrent:=10;
  Purplem.ycurrent:=5;
  pinkm.xcurrent:=10;
  pinkm.ycurrent:=7;
  greenm.xcurrent:=9;
  greenm.ycurrent:=6;
  redm.xcurrent:=11;
  redm.ycurrent:=6;
  Monstertimer.Enabled:=false;
  Pactimer.Enabled:=false;
  distimer.Enabled:=false;
    gamefield[10, 5,0] := false;
  gamefield[10, 7,0] := false;
  gamefield[9, 6,0] := false;
  gamefield[11, 6,0] := false;
  pacy.lives:=Pacy.lives-1;
  clocktimer.Enabled:=false;
  end;
     if (pacy.xcurrent=pinkm.xcurrent) and (Pacy.ycurrent=pinkm.ycurrent) then
  begin
    Pacy.disfield[4, 6] := True;
  Purplem.disfield[10, 5] := True;
  pinkm.disfield[10, 7] := True;
  greenm.disfield[9, 6] := True;
  redm.disfield[11, 6] := True;
  pacy.disfield[Pacy.xcurrent, pacy.ycurrent]:=false;
  Purplem.disfield[Purplem.xcurrent,Purplem.ycurrent]:=false;
  pinkm.disfield[pinkm.xcurrent,pinkm.ycurrent]:=false;
  greenm.disfield[greenm.xcurrent,greenm.ycurrent]:=false;
  redm.disfield[redm.xcurrent,redm.ycurrent]:=false;
   Purplem.lastdirection:=empty;
  pinkm.lastdirection:=empty;
  greenm.lastdirection:=empty;
  redm.lastdirection:=empty;
  pacy.xcurrent:=4;
  Pacy.ycurrent:=6;
  Purplem.xcurrent:=10;
  Purplem.ycurrent:=5;
  pinkm.xcurrent:=10;
  pinkm.ycurrent:=7;
  greenm.xcurrent:=9;
  greenm.ycurrent:=6;
  redm.xcurrent:=11;
  redm.ycurrent:=6;
  Monstertimer.Enabled:=false;
  Pactimer.Enabled:=false;
  distimer.Enabled:=false;
    gamefield[10, 5,0] := false;
  gamefield[10, 7,0] := false;
  gamefield[9, 6,0] := false;
  gamefield[11, 6,0] := false;
  pacy.lives:=Pacy.lives-1;
  clocktimer.Enabled:=false;
  end;
      T.DrawSquare(4,6,0);
  T.Invalidate;
  end else begin
      if (pacy.xcurrent=Purplem.xcurrent) and (Pacy.ycurrent=Purplem.ycurrent) then  begin
      Purplem.disfield[Purplem.xcurrent,Purplem.ycurrent]:=false;
      purplem.disfield[10, 5] := True;
       Purplem.xcurrent:=10;
  Purplem.ycurrent:=5;
   Purplem.lastdirection:=empty;
          gamefield[10, 5,0] := false;

      end;
      if (pacy.xcurrent=pinkm.xcurrent) and (Pacy.ycurrent=pinkm.ycurrent) then begin
      pinkm.disfield[10, 7] := True;
      pinkm.disfield[pinkm.xcurrent,pinkm.ycurrent]:=false;

  gamefield[10, 7,0] := false;

  pinkm.lastdirection:=empty;

       pinkm.xcurrent:=10;
  pinkm.ycurrent:=7;
      end;
      if (pacy.xcurrent=redm.xcurrent) and (Pacy.ycurrent=redm.ycurrent) then begin
       redm.disfield[redm.xcurrent,redm.ycurrent]:=false;
       redm.disfield[11, 6] := True;
       redm.xcurrent:=11;

  gamefield[11, 6,0] := false;
  redm.lastdirection:=empty;
  redm.ycurrent:=6;
      end;
      if (pacy.xcurrent=greenm.xcurrent) and (Pacy.ycurrent=greenm.ycurrent) then begin
          greenm.disfield[greenm.xcurrent,greenm.ycurrent]:=false;
          greenm.disfield[9, 6] := True;

  gamefield[9, 6,0] := false;

  greenm.lastdirection:=empty;

           greenm.xcurrent:=9;
  greenm.ycurrent:=6;
      end;

      T.invalidate;
  end;

end;



procedure TGameform.timstopstartClick(Sender: TObject);
begin
  if Pactimer.Enabled = False then
  begin
    Pactimer.Enabled := True;
    Monstertimer.Enabled := True;
    distimer.Enabled:=True;
    clocktimer.Enabled:=True;
  end
  else
  begin
     distimer.Enabled:=False;
    Pactimer.Enabled := False;
    Monstertimer.Enabled := False;
    clocktimer.Enabled:=False;
  end;
end;

end.
